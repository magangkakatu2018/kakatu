package com.iqbalfauzi.kakatu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.iqbalfauzi.kakatu.util.ConnectivityReceiver;
import com.iqbalfauzi.kakatu.util.SharedPrefManager;
import com.iqbalfauzi.kakatu.util.api.BaseApiService;
import com.iqbalfauzi.kakatu.util.api.UtilsApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    private static final String TAG = "LoginActivity";

    @Bind(R.id.input_email)
    EditText _emailText;
    @Bind(R.id.input_password)
    EditText _passwordText;
    @Bind(R.id.btn_login)
    Button _loginButton;

    ProgressDialog loading;
    Context mContext;
    BaseApiService mApiService;
    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mContext = this;
        mApiService = UtilsApi.getAPIService(); // meng-init yang ada di package apihelper

        checkSP();
        checkConnection();
        _emailText.addTextChangedListener(loginTextWatcher);
        _passwordText.addTextChangedListener(loginTextWatcher);
        clickLogin();
    }

    private TextWatcher loginTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String emailInput = _emailText.getText().toString().trim();
            String passwordInput = _passwordText.getText().toString().trim();
            _loginButton.setEnabled(!emailInput.isEmpty() && !passwordInput.isEmpty());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        Kakatu.getInstance().setConnectivityListener(this);
    }

    public void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        if (isConnected == false) {
            showSnack(isConnected);
        }
    }

    private void showSnack(boolean isConnected) {

        String message;
        int color;
        if (isConnected == false) {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
            Snackbar snackbar = Snackbar
                    .make(findViewById(R.id.btn_login), message, Snackbar.LENGTH_LONG);

            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void clickLogin() {
        _loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validate()) {
                    return;
                }
                checkConnection();
                requestLogin();
            }
        });
    }

    private void checkSP() {
        sharedPrefManager = new SharedPrefManager(this);
        // Code berikut berfungsi untuk mengecek session, Jika session true ( sudah login )
        // maka langsung memulai MainActivity.
        if (sharedPrefManager.getSpHasLogin()) {
            startActivity(new Intent(LoginActivity.this, PresentActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }
    }

    private void requestLogin() {
        mApiService.loginRequest1(_emailText.getText().toString(), _passwordText.getText().toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        final JSONObject jsonRESULTS = new JSONObject(response.body().string());
                        if (jsonRESULTS.getString("error").equals("false")) {
                            loading = ProgressDialog.show(mContext, null, "Authenticating...", true, false);
                            new android.os.Handler().postDelayed(
                                    new Runnable() {
                                        public void run() {
                                            loading.dismiss();
                                            // Jika login berhasil maka data nama yang ada di response API
                                            // akan diparsing ke activity selanjutnya.
//                                                    Toast.makeText(mContext, "Login Berhasil", Toast.LENGTH_SHORT).show();
                                            String id = null;
                                            String name = null;
                                            String email = null, fullName = null, tempatLahir = null, tglLahir = null,
                                                    gender = null, address = null, jabatan = null, foto = null;
                                            try {
                                                id = jsonRESULTS.getJSONObject("UserItem").getString("id_anggota");
                                                name = jsonRESULTS.getJSONObject("UserItem").getString("nama");
                                                email = jsonRESULTS.getJSONObject("UserItem").getString("email");
                                                fullName = jsonRESULTS.getJSONObject("UserItem").getString("nama_lengkap");
                                                tempatLahir = jsonRESULTS.getJSONObject("UserItem").getString("tempat_lahir");
                                                tglLahir = jsonRESULTS.getJSONObject("UserItem").getString("tgl_lahir");
                                                gender = jsonRESULTS.getJSONObject("UserItem").getString("jenis_kelamin");
                                                address = jsonRESULTS.getJSONObject("UserItem").getString("alamat");
                                                jabatan = jsonRESULTS.getJSONObject("UserItem").getString("jabatan");
                                                foto = jsonRESULTS.getJSONObject("UserItem").getString("foto_profile");
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            sharedPrefManager.saveSPStringId(SharedPrefManager.spID, id);
                                            sharedPrefManager.saveSPName(SharedPrefManager.spNama, name);
                                            sharedPrefManager.saveSpEmail(SharedPrefManager.spEmail, email);
                                            sharedPrefManager.saveSpFullName(SharedPrefManager.spFullName, fullName);
                                            sharedPrefManager.saveSpTempatLahir(SharedPrefManager.spTempatLahir, tempatLahir);
                                            sharedPrefManager.saveSpTglLahir(SharedPrefManager.spTglLahir, tglLahir);
                                            sharedPrefManager.saveSpGender(SharedPrefManager.spGender, gender);
                                            sharedPrefManager.saveSpAddress(SharedPrefManager.spAddress, address);
                                            sharedPrefManager.saveSpJabatan(SharedPrefManager.spJabatan, jabatan);
                                            sharedPrefManager.saveSpFoto(SharedPrefManager.spFoto, foto);
                                            Log.i(TAG, sharedPrefManager.getSpId() + "\n" + sharedPrefManager.getSpNama() + "\n"
                                                    + sharedPrefManager.getSpEmail() + "\n" + sharedPrefManager.getSpFullName() + "\n"
                                                    + sharedPrefManager.getSpAddress() + "\n" + sharedPrefManager.getSpJabatan() + "\n"
                                                    + sharedPrefManager.getSpFoto());
                                            // Shared Pref ini berfungsi untuk menjadi trigger session login
                                            sharedPrefManager.saveSPBooleanLogin(SharedPrefManager.SP_LOGIN, true);
                                            startActivity(new Intent(mContext, PresentActivity.class)
                                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                                            finish();
                                        }
                                    }, 3000);
                        } else {
                            // Jika login gagal
//                            String error_message = jsonRESULTS.getString("error_msg");
                            int color = Color.RED;
                            Snackbar snackbar = Snackbar
                                    .make(findViewById(R.id.btn_login), "Login Failed!", Snackbar.LENGTH_LONG);
                            View sbView = snackbar.getView();
                            TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                            textView.setTextColor(color);
                            snackbar.show();
                            _emailText.setError("Check Your Email!");
                            _passwordText.setError("Check Your Password!");
//                            Toast.makeText(mContext, "Login gagal.", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("debug", "onFailure: ERROR > " + t.toString());
                int color = Color.RED;
                Snackbar snackbar = Snackbar
                        .make(findViewById(R.id.btn_login), "Login Failed!", Snackbar.LENGTH_LONG);
                View sbView = snackbar.getView();
                TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(color);
                snackbar.show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public Boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("Check Your Email!");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.length() < 4) {
            _passwordText.setError("Check Your Password!");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    public void onReg(View view) {
        startActivity(new Intent(this, RegisterActivity.class));
    }
}
