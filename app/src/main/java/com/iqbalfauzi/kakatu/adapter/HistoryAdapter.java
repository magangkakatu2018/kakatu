package com.iqbalfauzi.kakatu.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.iqbalfauzi.kakatu.R;
import com.iqbalfauzi.kakatu.model.AttendanceModel;
import com.iqbalfauzi.kakatu.model.HistoryModel;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.Holder>{

    private List<HistoryModel> historyModels;
    private Context mContext;

    public HistoryAdapter(List<HistoryModel> historyModels, Context mContext) {
        this.historyModels = historyModels;
        this.mContext = mContext;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.list_item, null);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        HistoryModel model = historyModels.get(position);

        //loading the image imageView
//        Glide.with(mContext)
//                .load(model.getLinkProfileImage())
//                .into(holder.imageView);
//        Log.i("adapter", model.getLinkProfileImage());

        //set data kehadiran
        holder.tvNama.setText(model.getNama());
        holder.tvState.setText(model.getStatus());
        holder.tvTanggal.setText(model.getTanggal());
    }

    @Override
    public int getItemCount() {
        return historyModels.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {

        public TextView tvNama;
        public TextView tvState;
        public TextView tvTanggal;
        public CircleImageView imageView;

        public Holder(View itemView) {
            super(itemView);

            tvNama = itemView.findViewById(R.id.tv_ls_name);
            tvState = itemView.findViewById(R.id.tv_ls_state);
            tvTanggal = itemView.findViewById(R.id.tv_ls_date);

        }
    }
}
