package com.iqbalfauzi.kakatu.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.iqbalfauzi.kakatu.R;
import com.iqbalfauzi.kakatu.model.AttendanceModel;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.Holder>{

    private List<AttendanceModel> mListData;
    private Context mContext;

    public AttendanceAdapter(List<AttendanceModel> mListData, Context mContext) {
        this.mListData = mListData;
        this.mContext = mContext;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.grid_view, null);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        AttendanceModel model = mListData.get(position);

        //loading the image imageView
        Glide.with(mContext)
                .load(model.getLinkProfileImage())
                .into(holder.imageView);
        Log.i("adapter", model.getLinkProfileImage());

        //set data kehadiran
        holder.tvNama.setText(model.getNama());
        holder.tvState.setText(model.getStatus());
        holder.tvJamMasuk.setText(model.getJamMasuk());
    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {

        public TextView tvNama;
        public TextView tvState;
        public TextView tvJamMasuk;
        public CircleImageView imageView;

        public Holder(View itemView) {
            super(itemView);

            tvNama = itemView.findViewById(R.id.txt_nama);
            tvState = itemView.findViewById(R.id.txt_state);
            tvJamMasuk = itemView.findViewById(R.id.txt_time_come);
            imageView = itemView.findViewById(R.id.img_user_thumbnail);

        }
    }
}
