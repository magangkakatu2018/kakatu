package com.iqbalfauzi.kakatu;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.iqbalfauzi.kakatu.fragments.DashboardFragment;
import com.iqbalfauzi.kakatu.fragments.HistoryFragment;
import com.iqbalfauzi.kakatu.fragments.ProfileFragment;
import com.iqbalfauzi.kakatu.util.SharedPrefManager;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    Button btnEdit, btnLogout;
    TabLayout tabLayout;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private android.app.Fragment fragment = null;
    private android.app.FragmentManager fragmentManager;

    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponent();
        sharedPrefManager = new SharedPrefManager(this);

        btnEdit.setVisibility(View.GONE);
        btnLogout.setVisibility(View.VISIBLE);
        toolbar.setTitle("Dashboard");
        setSupportActionBar(toolbar);

        setFragmentAdapter();
        onEditButton();
        onLogoutButton();
    }

    private void setFragmentAdapter() {
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout.setupWithViewPager(mViewPager);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    toolbar.setTitle("Dashboard");
                    btnEdit.setVisibility(View.GONE);
                } else if (position == 1) {
                    toolbar.setTitle("History");
                    btnEdit.setVisibility(View.GONE);
                } else if (position == 2) {
                    toolbar.setTitle("Profile");
                    btnEdit.setVisibility(View.VISIBLE);
                }

//                else if (position == 1) {
//                    toolbar.setTitle("Data Absen");
//                    btnEdit.setVisibility(View.GONE);
//                }
//
//                else if (position == 2) {
//                    toolbar.setTitle("Profil");
//                    btnEdit.setVisibility(View.VISIBLE);
//                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_dashboard);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_data);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_profile);
//        tabLayout.getTabAt(2).setIcon(R.drawable.ic_profile);
    }

    private void onLogoutButton() {
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPrefManager.saveSPBooleanLogin(SharedPrefManager.SP_LOGIN, false);
                sharedPrefManager.saveSPBooleanAbsen(sharedPrefManager.SP_ABSEN, false);
                sharedPrefManager.saveSPStringId(sharedPrefManager.spID, "");
                sharedPrefManager.saveSPStringIDAbsen(sharedPrefManager.spIDAbsen, "");

                startActivity(new Intent(MainActivity.this, LoginActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
    }

    private void initComponent() {
        toolbar = findViewById(R.id.toolbar);
        btnEdit = findViewById(R.id.btn_edit);
        btnLogout = findViewById(R.id.btn_logout);
        tabLayout = findViewById(R.id.tabs);
    }

    private void onEditButton() {
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, EditActivity.class);
                startActivity(intent);
            }
        });
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    DashboardFragment tab1 = new DashboardFragment();
                    return tab1;
                case 1:
                    HistoryFragment tab2 = new HistoryFragment();
                    return tab2;
                 case 2:
                    ProfileFragment tab3 = new ProfileFragment();
                    return tab3;
//                 case 1:
//                    HistoryFragment tab2 = new HistoryFragment();
//                    return tab2;
//                case 2:
//                    ProfileFragment tab3 = new ProfileFragment();
//                    return tab3;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }


    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

}
