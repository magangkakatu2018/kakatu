package com.iqbalfauzi.kakatu.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.iqbalfauzi.kakatu.PresentActivity;
import com.iqbalfauzi.kakatu.R;
import com.iqbalfauzi.kakatu.adapter.AttendanceAdapter;
import com.iqbalfauzi.kakatu.model.AttendanceModel;
import com.iqbalfauzi.kakatu.model.PieChartModel;
import com.iqbalfauzi.kakatu.util.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.ButterKnife;

import static com.github.mikephil.charting.utils.ColorTemplate.MATERIAL_COLORS;
import static com.github.mikephil.charting.utils.ColorTemplate.createColors;
import static com.github.mikephil.charting.utils.ColorTemplate.rgb;


/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {

    private Handler timeHandler = new Handler();
    String TAG = "DashboardFragment";

    private String urlReycler = "http://testabsensi.xyz/api/data/GaleryAbsen";
    private RecyclerView recyclerView;
    private List<AttendanceModel> mListData;
    private AttendanceAdapter mAdapter;
    private ProgressDialog mProgressDialog;
    SharedPrefManager sharedPrefManager;
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    String dateToday = dateFormat.format(calendar.getTime());
//    @Bind(R.id.btn_absen)
//    FloatingActionButton btnPulang;
//    @Bind(R.id.progress_bar)
//    ProgressBar progressBar;

    public DashboardFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        sharedPrefManager = new SharedPrefManager(getContext());
        ButterKnife.bind(rootView);

        showTime(rootView);
        onLeft(rootView);
//        onPulang(rootView);
        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setMessage("Loading ...");
        mProgressDialog.show();
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        getDataChart(rootView);
                        viewRecyclerData(rootView);
                        requestIDAbsen();
                        mProgressDialog.dismiss();
                    }
                }, 3000);
        onSwipeRefresh(rootView);

        return rootView;
    }

//    private void onPulang(View view) {
//        FloatingActionButton btnPulang = (FloatingActionButton)view.findViewById(R.id.btn_absen);
//        btnPulang.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sharedPrefManager.saveSPBooleanAbsen(sharedPrefManager.SP_ABSEN, false);
//                sharedPrefManager.saveSPStringIDAbsen(sharedPrefManager.spIDAbsen, "");
//
//                startActivity(new Intent(getContext(), PresentActivity.class)
//                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
//                getActivity().finish();
//            }
//        });
//    }

    private void onSwipeRefresh(final View view) {
        final SwipeRefreshLayout swipeRefreshLayout = view.findViewById(R.id.swp_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                refreshItem();
            }

            private void refreshItem() {
                getDataChart(view);
                viewRecyclerData(view);
                showTime(view);
                onItemLoad();
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 3000);
            }

            private void onItemLoad() {
                swipeRefreshLayout.setRefreshing(false);
            }

        });
    }

    private void requestIDAbsen() {
        final String idAnggota = sharedPrefManager.getSpId();
        String url = "http://testabsensi.xyz/api/data/DataAbsen";
        final StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("string", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("Data");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);

                                String idAbsen = object.getString("id_absen");
                                sharedPrefManager.saveSPStringIDAbsen(SharedPrefManager.spIDAbsen, idAbsen);
                                Log.d(TAG, sharedPrefManager.getSpIdAbsen());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                },

                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

            protected Map<String, String> getParams() {
                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("id_anggota", idAnggota); //Add the data you'd like to send to the server.
                MyData.put("tanggal", dateToday);
                return MyData;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(request);
    }

    private void getDataChart(final View view) {
//        ArrayList yAxis = null;
//        final ArrayList yValues = new ArrayList<>();
//        final ArrayList xAxis1 = new ArrayList<>();
//        BarEntry values;
//        chart = (BarChart) view.findViewById(R.id.chart_absen);
        String urlChart = "http://testabsensi.xyz/api/data/Chart";

        final StringRequest request = new StringRequest(Request.Method.POST, urlChart,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("string", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("Chart");

                            List<PieEntry> pieEntries = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String labelName = object.getString("label");
                                Float valueCount = Float.valueOf(object.getString("value"));

                                PieChartModel chartModel = new PieChartModel();
                                chartModel.setLabel(labelName);
                                chartModel.setValue(valueCount);

                                Float jumlahAbsen[] = {chartModel.getValue()};
                                String label[] = {chartModel.getLabel()};

                                for (int x = 0; x < jumlahAbsen.length; x++) {
                                    pieEntries.add(new PieEntry(jumlahAbsen[x], label[x]));
                                }
                            }

                            PieDataSet dataSet = new PieDataSet(pieEntries, "");
                            dataSet.setColors(createColors(MATERIAL_COLORS));
                            PieData data = new PieData(dataSet);
                            dataSet.setFormSize(16f);
                            dataSet.setValueTextSize(16f);

                            dataSet.setSliceSpace(2);

                            //tampilkan piechart
                            PieChart chart = (PieChart) view.findViewById(R.id.chart_absen);
                            chart.setData(data);
                            chart.animateY(1000);
                            chart.invalidate();

                            Description description = new Description();
                            description.setText("");
                            chart.setDescription(description);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                },

                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });


        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(request);
    }

    public static final int[] colorPresent = {
            rgb("#00C0EF"), rgb("#F56954"), rgb("#F39C12"), rgb("#3498db")
    };

    public void viewRecyclerData(View view) {
        recyclerView = view.findViewById(R.id.recycler_view);
        mListData = new ArrayList<>();
        getPresentData();
    }

    private void getPresentData() {
        final StringRequest request = new StringRequest(Request.Method.GET, urlReycler,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();
                        iniData(response);
                    }
                },

                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(request);
    }

    private void iniData(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("Jumlah");

            //looping utk array
            for (int i = 0; i < jsonArray.length(); i++) {
                //get json berdasarkan banyaknya data (index i)
                JSONObject object = jsonArray.getJSONObject(i);

                //get data berdasarkan attribute yang ada dijsonnya (harus sama)
                String namaUser = object.getString("nama");
                String status = object.getString("status");
                String jamMasuk = object.getString("jam_masuk");
                String profileImage = object.getString("link_profile_image");

                //add data ke modelnya
                AttendanceModel attendanceModel = new AttendanceModel();
                attendanceModel.setNama(namaUser);
                attendanceModel.setLinkProfileImage(profileImage);
                attendanceModel.setStatus(status);
                attendanceModel.setJamMasuk(jamMasuk);

                //add model ke list
                mListData.add(attendanceModel);

                //passing data list ke adapter
                mAdapter = new AttendanceAdapter(mListData, getContext());
                mAdapter.notifyDataSetChanged();
                recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(mAdapter);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void showTime(final View view) {
        Runnable timeRunnable = new Runnable() {
            @Override
            public void run() {
                Calendar cl = Calendar.getInstance();
                SimpleDateFormat showTime = new SimpleDateFormat(", d/M/yyy | HH:m:s");
                String stringDate = showTime.format(cl.getTime());
                TextView dateAndTime = (TextView) view.findViewById(R.id.date_time);
                dateAndTime.setText(new SimpleDateFormat("EE", Locale.getDefault()).format(cl.getTime()) + stringDate);

                timeHandler.postDelayed(this, 1000);
            }
        };
        timeHandler.post(timeRunnable);
    }

    public void onLeft(View view) {
        final FloatingActionButton btnAbsen = view.findViewById(R.id.btn_absen);
        btnAbsen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get calendar set to the current date and time
                Calendar cal = Calendar.getInstance();

                cal.set(Calendar.HOUR_OF_DAY, 17);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);

                boolean afterFive = Calendar.getInstance().after(cal);

                if (afterFive) {
//                    Snackbar.make(v, "Kamu sudah bisa pulang.", Snackbar.LENGTH_LONG).show();
                    sharedPrefManager.saveSPBooleanAbsen(sharedPrefManager.SP_ABSEN, false);
                    sharedPrefManager.saveSPStringIDAbsen(sharedPrefManager.spIDAbsen, "");

                    startActivity(new Intent(getContext(), PresentActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    getActivity().finish();
                } else {
                    Snackbar.make(v, "Kamu belum bisa pulang", Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

}
