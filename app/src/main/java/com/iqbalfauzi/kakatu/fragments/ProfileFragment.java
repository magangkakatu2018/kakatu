package com.iqbalfauzi.kakatu.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iqbalfauzi.kakatu.R;
import com.iqbalfauzi.kakatu.util.SharedPrefManager;
import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    Context mContext;
    SharedPrefManager sharedPrefManager;

//    @Bind(R.id.profile_image)
//    CircleImageView profileImage;
//    @Bind(R.id.tv_name_profile)
//    TextView tvFullName;
//    @Bind(R.id.tv_email)
//    TextView tvEmail;
//    @Bind(R.id.tv_jabatan)
//    TextView tvJabatan;
//    @Bind(R.id.tv_born)
//    TextView tvBorn;
//    @Bind(R.id.tv_gender)
//    TextView tvGender;
//    @Bind(R.id.tv_address)
//    TextView tvAddress;
    CircleImageView profileImage;
    TextView tvFullName;
    TextView tvEmail;
    TextView tvJabatan;
    TextView tvBorn;
    TextView tvGender;
    TextView tvAddress;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        sharedPrefManager = new SharedPrefManager(getContext());

        init(rootView);
        showProfile(rootView);

        return rootView;
    }

    private void init(View view) {
        profileImage = view.findViewById(R.id.profile_image);
        tvFullName = view.findViewById(R.id.tv_name_profile);
        tvBorn = view.findViewById(R.id.tv_born);
        tvGender = view.findViewById(R.id.tv_gender);
        tvEmail = view.findViewById(R.id.tv_email);
        tvJabatan = view.findViewById(R.id.tv_jabatan);
        tvAddress = view.findViewById(R.id.tv_address);
    }

    private void showProfile(View view) {

        String urlImage = "http://testabsensi.xyz/assets/dist/img/anggota/resize/"+sharedPrefManager.getSpFoto();
        Glide.with(getContext())
                .load(urlImage)
                .into(profileImage);
        tvFullName.setText(sharedPrefManager.getSpFullName());
        String jenisKelamin = sharedPrefManager.getSpGender();
        if (jenisKelamin.equals("L")) {
            tvGender.setText("Laki-laki");
        } else if (jenisKelamin.equals("P")) {
            tvGender.setText("Perempuan");
        }
        tvEmail.setText(sharedPrefManager.getSpEmail());
        tvJabatan.setText(sharedPrefManager.getSpJabatan());
        dateBornConverter();
        tvAddress.setText(sharedPrefManager.getSpAddress());
    }

    private void dateBornConverter() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateInString = sharedPrefManager.getSpTglLahir();
        try {
            Date myDate = dateFormat.parse(dateInString);
            dateFormat.applyPattern("dd MMMM yyyy");
            String myDateString = dateFormat.format(myDate);
            tvBorn.setText(sharedPrefManager.getSpTempatLahir() + ", " + myDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
