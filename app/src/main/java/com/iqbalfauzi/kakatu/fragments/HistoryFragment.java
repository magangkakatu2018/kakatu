package com.iqbalfauzi.kakatu.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.iqbalfauzi.kakatu.R;
import com.iqbalfauzi.kakatu.adapter.HistoryAdapter;
import com.iqbalfauzi.kakatu.model.HistoryModel;
import com.iqbalfauzi.kakatu.util.SharedPrefManager;
import com.iqbalfauzi.kakatu.util.api.BaseApiService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.github.mikephil.charting.utils.ColorTemplate.createColors;


/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {

    private Handler timeHandler = new Handler();

    private String urlReycler = "http://testabsensi.xyz/api/data/GaleryAbsen";
    private RecyclerView recyclerView;
    private List<HistoryModel> mListData;
    private HistoryAdapter mAdapter;
    private ProgressDialog mProgressDialog;
    SharedPrefManager sharedPrefManager;
    BaseApiService mApiService;
    String email = "iqbal@gmail.com";
    String password = "juragan";
//    @Bind(R.id.progress_bar)
//    ProgressBar progressBar;

    public HistoryFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_data, container, false);
        sharedPrefManager = new SharedPrefManager(getContext());

//        mProgressDialog = new ProgressDialog(getContext());
//        mProgressDialog.setMessage("Loading ...");
//        mProgressDialog.show();
//        new Handler().postDelayed(
//                new Runnable() {
//                    @Override
//                    public void run() {
//                        viewRecyclerData(rootView);
//                        mProgressDialog.dismiss();
//                    }
//                }, 3000);

        viewRecyclerData(rootView);

        return rootView;
    }

    public void viewRecyclerData(View view) {
        recyclerView = view.findViewById(R.id.recycler_view_history);
        mListData = new ArrayList<>();
        requestData();
    }

    private void requestData() {
        final String idAnggota = sharedPrefManager.getSpId();
        String url = "http://testabsensi.xyz/api/data/History";
        final StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("string", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("History");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);

                                String nama = object.getString("nama");
                                String status = object.getString("status");
                                String tanggal = object.getString("tanggal");

                                HistoryModel historyModel = new HistoryModel();
                                historyModel.setNama(nama);
                                historyModel.setStatus(status);
                                historyModel.setTanggal(tanggal);

                                mListData.add(historyModel);

                                mAdapter = new HistoryAdapter(mListData, getContext());
                                mAdapter.notifyDataSetChanged();
                                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                recyclerView.setAdapter(mAdapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                },

                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

            protected Map<String, String> getParams() {
                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("id_anggota", idAnggota); //Add the data you'd like to send to the server.
                return MyData;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(request);
    }
}
