package com.iqbalfauzi.kakatu;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.iqbalfauzi.kakatu.kehadiran.CutiActivity;
import com.iqbalfauzi.kakatu.kehadiran.IzinActivity;
import com.iqbalfauzi.kakatu.kehadiran.RemoteActivity;
import com.iqbalfauzi.kakatu.kehadiran.SakitActivity;
import com.iqbalfauzi.kakatu.kehadiran.TugasActivity;
import com.iqbalfauzi.kakatu.util.ConnectivityReceiver;
import com.iqbalfauzi.kakatu.util.SharedPrefManager;
import com.iqbalfauzi.kakatu.util.api.BaseApiService;
import com.iqbalfauzi.kakatu.util.api.UtilsApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PresentActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    private static final String TAG = "PresentActivity";

    private Handler timeHandler = new Handler();
    static final int REQUEST_LOCATION = 1;
    double latti;
    double longi;
    Location kantor = new Location("");
    Location lokasiKu = new Location("");
    float distanceInMeters;
    private LocationManager locationManager;
    Context mContext;
    SharedPrefManager sharedPrefManager;
    BaseApiService mApiService;
    Calendar cl;
    SimpleDateFormat showDate;
    SimpleDateFormat showTime;
    String macServer1, macAddress;
    WifiInfo wifiInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absen);
        //check wifi validation
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiInfo = wifiManager.getConnectionInfo();
        mContext = this;
        mApiService = UtilsApi.getAPIService(); // meng-init yang ada di package apihelper
        sharedPrefManager = new SharedPrefManager(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Log.i(TAG, sharedPrefManager.getSpId() + "\n" + sharedPrefManager.getSpNama() + "\n"
                + sharedPrefManager.getSpEmail() + "\n" + sharedPrefManager.getSpFullName() + "\n"
                + sharedPrefManager.getSpAddress() + "\n" + sharedPrefManager.getSpJabatan() + "\n"
                + sharedPrefManager.getSpFoto());

        checkSession();
        timeRunning();
        clickPresent();
    }

    private void showSnack(boolean isConnected) {

        String message;
        int color;
        if (isConnected == false) {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
            Snackbar snackbar = Snackbar
                    .make(findViewById(R.id.card_hadir), message, Snackbar.LENGTH_LONG);

            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void checkSession() {
        if (sharedPrefManager.getSpHasLogin() && sharedPrefManager.getSpHasAbsen()) {
            startActivity(new Intent(PresentActivity.this, MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        } else {
            //Method getLocation() adalah method yang digunakan untuk mendapatkan lokasi berupa latitude dan longitude
            officeLocation();
            getLocation();
        }
    }

    private void clickPresent() {
        //Ketika cardPresent di click maka akan muncul AlertDialog
        //Inisialisasi CardView
        CardView cardPresent = findViewById(R.id.card_hadir);
        cardPresent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!locationValidate()) {
                    return;
                }
                if (!wifiValidate()) {
                    return;
                }
                boolean isConnected = ConnectivityReceiver.isConnected();
                if (!isConnected) {
                    showSnack(isConnected);
                    return;
                }
                shareWA();
            }
        });
    }

    private boolean locationValidate() {

        boolean valid = true;

        if (distanceInMeters > 30) {
            valid = false;
            int color = Color.WHITE;
            Snackbar snackbar = Snackbar
                    .make(findViewById(R.id.card_hadir), "Anda belum berada di kantor!", Snackbar.LENGTH_LONG);
            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }

        return valid;
    }

    private boolean wifiValidate() {

        boolean valid = true;
        macServer1 = wifiInfo.getBSSID(); //get macAddress Access Point Server
        macAddress = "3c:f8:08:21:fb:c8";
//        Log.d("BSSID", macServer1);

        if (!macAddress.equals(macServer1)) {
            valid = false;
            int color = Color.WHITE;
            Snackbar snackbar = Snackbar
                    .make(findViewById(R.id.card_hadir), "Anda tidak terhubung dengan WiFi kantor!", Snackbar.LENGTH_LONG);
            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }

        return valid;
    }

    private void showAlert() {
        int color = Color.WHITE;
        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.card_hadir), "Anda tidak terhubung dengan WiFi kantor!", Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }

    public void timeRunning(){
        //Dibawah ini adalah code untuk menampilkan waktu
        Runnable timeRunnable = new Runnable() {
            @Override
            public void run() {
                cl = Calendar.getInstance();
                showDate = new SimpleDateFormat(", d/M/yyy | HH:m:s");
                String stringDate = showDate.format(cl.getTime());
                TextView dateAndTime = (TextView) findViewById(R.id.date);
                dateAndTime.setText(new SimpleDateFormat("EE", Locale.getDefault()).format(cl.getTime()) + stringDate);

                timeHandler.postDelayed(this, 1000);
            }
        };
        timeHandler.post(timeRunnable);
    }

    public void shareWA() {
        /**
         * Code dibawah ini digunakan untuk mengirim data ke WhatsApp beserta dengan koordinat
         * latitude dan longitude.
         * Mengirim data ini menggunakan Implisit Intent.
         */
        showTime = new SimpleDateFormat("HH:mm");
        String time = showTime.format(cl.getTime());
        String uri = "https://maps.google.com/?q=" + latti + "," + longi;
        String myName = sharedPrefManager.getSpFullName();
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, "HADIR\n --- \nSaya, " + myName
                + " sudah hadir di kantor hari ini pukul " + time);
        try {
            startActivityForResult(whatsappIntent, 1);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(PresentActivity.this, "Whatsapp have not been installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void requestPresent() {
        cl = Calendar.getInstance();
        showDate = new SimpleDateFormat("yyyy-MM-dd");
        showTime = new SimpleDateFormat("HH:mm");

        String idAnggota = sharedPrefManager.getSpId();
        String idStatus = sharedPrefManager.getSPPresent();
        final String strDate = showDate.format(cl.getTime());
        String strTime = showTime.format(cl.getTime());
        String latitude = String.valueOf(latti);
        String longitude = String.valueOf(longi);

        mApiService.presentPost(
                idAnggota, idStatus, strDate, strTime, latitude, longitude)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
//                            loading.dismiss();
//                            Toast.makeText(mContext, strDate, Toast.LENGTH_SHORT).show();
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("error").equals("false")) {

                                    // Jika login berhasil maka data nama yang ada di response API
                                    // akan diparsing ke activity selanjutnya.
//                                    Toast.makeText(mContext, "Login Berhasil", Toast.LENGTH_SHORT).show();
                                    String id = jsonRESULTS.getJSONObject("DetailAbsen").getString("id_absen");

//                                    sharedPrefManager.saveSPStringId(SharedPrefManager.spID, id);
                                    sharedPrefManager.saveSPStringIDAbsen(SharedPrefManager.spIDAbsen, id);
                                    Log.i(TAG, sharedPrefManager.getSpIdAbsen());
                                    // Shared Pref ini berfungsi untuk menjadi trigger session login
                                    sharedPrefManager.saveSPBooleanAbsen(SharedPrefManager.SP_ABSEN, true);
                                } else {
                                    int color = Color.RED;
                                    Snackbar snackbar = Snackbar
                                            .make(findViewById(R.id.card_hadir), "Submit Failed!", Snackbar.LENGTH_LONG);
                                    View sbView = snackbar.getView();
                                    TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                                    textView.setTextColor(color);
                                    snackbar.show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("debug", "onFailure: ERROR > " + t.toString());
                        int color = Color.RED;
                        Snackbar snackbar = Snackbar
                                .make(findViewById(R.id.card_hadir), "Submit Failed!", Snackbar.LENGTH_LONG);
                        View sbView = snackbar.getView();
                        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(color);
                        snackbar.show();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //jika requestCode nya 1 dan hasilnya OK maka setelah share ke WA akan lanjut masuk ke MainActivity dan dialognya akan di close
        if (requestCode == 1 && resultCode == RESULT_OK) {
            requestPresent();
            startActivity(new Intent(PresentActivity.this, MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }
    }

    public void onTugas(View view) {
        startActivity(new Intent(PresentActivity.this, TugasActivity.class));
    }

    public void onSakit(View view) {
        startActivity(new Intent(PresentActivity.this, SakitActivity.class));
    }

    public void onIzin(View view) {
        startActivity(new Intent(PresentActivity.this, IzinActivity.class));
    }

    public void onCuti(View view) {
        startActivity(new Intent(PresentActivity.this, CutiActivity.class));
    }

    public void onRemote(View view) {
        startActivity(new Intent(PresentActivity.this, RemoteActivity.class));
    }

    public void getLocation() {
        //meminta akses location
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        } else {
            final Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (location != null) {
                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Getting Location...");
                progressDialog.show();
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                latti = location.getLatitude();
                                longi = location.getLongitude();
                                lokasiKu.setLatitude(latti);
                                lokasiKu.setLongitude(longi);
                                progressDialog.dismiss();
                            }
                        },3000);
            }
        }
        return;
    }

    private void officeLocation() {
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
//
//        } else {
//            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//
//            if (location != null) {
                kantor.setLatitude(-7.3549444);
                kantor.setLongitude(108.2207627);
//            }
//        }
//        return;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_LOCATION:
                getLocation();
                break;
        }
    }

}