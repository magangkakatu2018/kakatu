package com.iqbalfauzi.kakatu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class RegisterActivity extends AppCompatActivity {

    EditText edtFullName, edtEmail, edtAddress, edtBorn, edtBornDate, edtPassword, edtCnfPassword;
    RadioGroup rbGender;
    RadioButton rbMale, rbFemale;
    Button btnBack, btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
        onBack();

    }

    public void onBack() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void init() {
        edtFullName = findViewById(R.id.edt_fullname);
        edtEmail = findViewById(R.id.edt_email);
        edtAddress = findViewById(R.id.edt_address);
        edtBorn = findViewById(R.id.edt_born);
        edtBornDate = findViewById(R.id.edt_borndate);
        edtPassword = findViewById(R.id.edt_passwd);
        edtCnfPassword = findViewById(R.id.edt_confirm_passwd);

        rbGender = findViewById(R.id.rb_gender);
        rbMale = findViewById(R.id.rb_male);
        rbFemale = findViewById(R.id.rb_female);

        btnBack = findViewById(R.id.btn_back_reg);
        btnSave = findViewById(R.id.btn_save_reg);
    }

}
