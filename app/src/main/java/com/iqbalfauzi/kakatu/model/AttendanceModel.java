package com.iqbalfauzi.kakatu.model;

import android.view.View;

public class AttendanceModel {

    private String idAbsen;
    private String idANggota;
    private String jamMasuk;
    private String jamKeluar;
    private String nama;
    private String status;
    private String foto;
    private String linkProfileImage;

    public AttendanceModel(String idAbsen, String idANggota, String jamMasuk, String jamKeluar, String nama, String status, String foto, String linkProfileImage) {
        this.idAbsen = idAbsen;
        this.idANggota = idANggota;
        this.jamMasuk = jamMasuk;
        this.jamKeluar = jamKeluar;
        this.nama = nama;
        this.status = status;
        this.foto = foto;
        this.linkProfileImage = linkProfileImage;
    }

    public AttendanceModel() {

    }

    public String getIdAbsen() {
        return idAbsen;
    }

    public void setIdAbsen(String idAbsen) {
        this.idAbsen = idAbsen;
    }

    public String getIdANggota() {
        return idANggota;
    }

    public void setIdANggota(String idANggota) {
        this.idANggota = idANggota;
    }

    public String getJamMasuk() {
        return jamMasuk;
    }

    public void setJamMasuk(String jamMasuk) {
        this.jamMasuk = jamMasuk;
    }

    public String getJamKeluar() {
        return jamKeluar;
    }

    public void setJamKeluar(String jamKeluar) {
        this.jamKeluar = jamKeluar;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLinkProfileImage() {
        return linkProfileImage;
    }

    public void setLinkProfileImage(String linkProfileImage) {
        this.linkProfileImage = linkProfileImage;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
