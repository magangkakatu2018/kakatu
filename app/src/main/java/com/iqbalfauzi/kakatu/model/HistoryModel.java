package com.iqbalfauzi.kakatu.model;

public class HistoryModel {

    private String nama;
    private String status;
    private String tanggal;


    public HistoryModel(String nama, String status, String tanggal) {
        this.nama = nama;
        this.status = status;
        this.tanggal = tanggal;
    }

    public HistoryModel() {

    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
