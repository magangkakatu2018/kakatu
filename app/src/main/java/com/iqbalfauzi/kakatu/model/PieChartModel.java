package com.iqbalfauzi.kakatu.model;

public class PieChartModel {

   private Float value;
   private String label;

    public PieChartModel(Float value, String label) {
        this.value = value;
        this.label = label;
    }

    public PieChartModel() {

    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
