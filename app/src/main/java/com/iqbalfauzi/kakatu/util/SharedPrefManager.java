package com.iqbalfauzi.kakatu.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by iqbal on 09/03/18.
 */

public class SharedPrefManager {

    public static final String SP_KINEST = "spKinest";

    public static final String spID = "spId";
    public static final String spNama = "spNama";
    public static final String spEmail = "spEmail";
    public static final String spFullName = "spFullName";
    public static final String spTempatLahir = "spTempatLahir";
    public static final String spTglLahir = "spTglLahir";
    public static final String spGender = "spGender";
    public static final String spAddress = "spAddress";
    public static final String spJabatan = "spJabatan";
    public static final String spFoto = "spFoto";
    public static final String spIDAbsen = "spIdAbsen";
    public static final String SP_LOGIN = "spHasLogin";
    public static final String SP_ABSEN = "spHasAbsen";
    public static final String SP_PRESENT = "1";
    public static final String SP_TASK = "2";
    public static final String SP_SICK = "3";
    public static final String SP_PERMIT = "4";
    public static final String SP_LEAVE = "5";
    public static final String SP_REMOTE = "7";

    SharedPreferences sp;
    SharedPreferences.Editor spEditor;

    public SharedPrefManager(Context context) {
        sp = context.getSharedPreferences(SP_KINEST, Context.MODE_PRIVATE);
        spEditor = sp.edit();
    }

    //sp untuk menyimpan data user
    public void saveSPStringId(String keySP, String value) {
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSPName(String keySP, String value) {
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSpEmail(String keySP, String value) {
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSpFullName(String keySP, String value) {
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSpTempatLahir(String keySP, String value) {
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSpTglLahir(String keySP, String value) {
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSpGender(String keySP, String value) {
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSpAddress(String keySP, String value) {
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSpJabatan(String keySP, String value) {
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSpFoto(String keySP, String value) {
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSPStringIDAbsen(String keySP, String value) {
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSPBooleanLogin(String keySP, boolean value) {
        spEditor.putBoolean(keySP, value);
        spEditor.commit();
    }

    public void saveSPBooleanAbsen(String keySP, boolean value) {
        spEditor.putBoolean(keySP, value);
        spEditor.commit();
    }

    //SP untuk mengambil value absensi
    public String getSPPresent() {
        return sp.getString(SP_PRESENT, "1");
    }

    public String getSPTask() {
        return sp.getString(SP_TASK, "2");
    }

    public String getSPSick() {
        return sp.getString(SP_SICK, "3");
    }

    public String getSPPermit() {
        return sp.getString(SP_PERMIT, "4");
    }

    public String getSPLeave() {
        return sp.getString(SP_LEAVE, "5");
    }

    public String getSPRemote() {
        return sp.getString(SP_REMOTE, "7");
    }

    //SP untuk mengambil data user
    public String getSpIdAbsen() {
        return sp.getString(spIDAbsen, "");
    }

    public String getSpId() {
        return sp.getString(spID, "");
    }

    public String getSpNama() {
        return sp.getString(spNama, "");
    }

    public String getSpEmail() {
        return sp.getString(spEmail, "");
    }

    public String getSpFullName() {
        return sp.getString(spFullName, "");
    }

    public String getSpTempatLahir() {
        return sp.getString(spTempatLahir, "");
    }

    public String getSpTglLahir() {
        return sp.getString(spTglLahir, "");
    }

    public String getSpGender() {
        return sp.getString(spGender, "");
    }

    public String getSpAddress() {
        return sp.getString(spAddress, "");
    }

    public String getSpJabatan() {
        return sp.getString(spJabatan, "");
    }

    public String getSpFoto() {
        return sp.getString(spFoto, "");
    }

    //sp untuk cek validasi Login dan absensi
    public Boolean getSpHasLogin() {
        return sp.getBoolean(SP_LOGIN, false);
    }

    public Boolean getSpHasAbsen() {
        return sp.getBoolean(SP_ABSEN, false);
    }
}
