package com.iqbalfauzi.kakatu.util.api;

/**
 * Created by iqbal on 09/03/18.
 */

public class UtilsApi {

    // 10.0.2.2 ini adalah localhost.
    // Mendeklarasikan Interface BaseApiService
    public static final String BASE_URL_API = "http://testabsensi.xyz/";

    public static BaseApiService getAPIService(){
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiService.class);
    }
}
