package com.iqbalfauzi.kakatu;

import android.app.Application;

import com.iqbalfauzi.kakatu.util.ConnectivityReceiver;

public class Kakatu extends Application {
    private static Kakatu mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
    }

    public static synchronized Kakatu getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }
}
